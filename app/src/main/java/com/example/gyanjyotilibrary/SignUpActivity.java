package com.example.gyanjyotilibrary;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener{

    private Button btnLoginSignup;
    private EditText username, email_id, password, re_password;
    DBHelper DB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        username=(EditText)findViewById(R.id.NameField);
        email_id=(EditText)findViewById(R.id.emailField);
        password=(EditText)findViewById(R.id.passwordField);
        re_password=(EditText)findViewById(R.id.repassword);
        btnLoginSignup = (Button)findViewById(R.id.loginSignupButton);
        btnLoginSignup.setOnClickListener(this);

        DB=new DBHelper(this);

        btnLoginSignup.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                String user=username.getText().toString();
                String emailID=email_id.getText().toString();
                String pass=password.getText().toString();
                String repass=re_password.getText().toString();

                if (user==("")|| emailID==("")|| pass==("")|| repass==("")){
                    Toast.makeText(SignUpActivity.this,"Please enter all fields !",Toast.LENGTH_SHORT).show();
                }
                else {
                    if (pass.equals(repass)){
                        Boolean checkemail=DB.checkemailID(emailID);
                        if (checkemail==false){
                            Boolean insert=DB.insertData(emailID,pass);
                            if (insert==true){
                                Toast.makeText(SignUpActivity.this,"User Added Successfully",Toast.LENGTH_SHORT).show();
                                Intent intent=new Intent(getApplicationContext(),StudentActivity.class);
                                startActivity(intent);
                            }
                            else {
                                Toast.makeText(SignUpActivity.this, "Adding User Failed !", Toast.LENGTH_SHORT).show();
                            }
                        }
                        else {
                            Toast.makeText(SignUpActivity.this, "User Already exists !", Toast.LENGTH_SHORT).show();
                        }
                    }
                    else{
                        Toast.makeText(SignUpActivity.this, "Incorrect Password !", Toast.LENGTH_SHORT).show();
                    }
                }

                Intent intent=new Intent(getApplicationContext(),StudentLoginActivity.class);
                startActivity(intent);
            }
        });

    }

    @Override
    public void onClick(View v){
        if (v.getId()==R.id.loginSignupButton){
            Log.d("SignUp","Clicked SignUp");

            startActivity(new Intent(SignUpActivity.this,LoginActivity.class));
        }
    }
}
