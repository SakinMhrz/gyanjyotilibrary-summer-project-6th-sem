package com.example.gyanjyotilibrary;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class StudentLoginActivity extends AppCompatActivity {

    private EditText loginEmail, loginPassword;

    private Button loginStudent;

    DBHelper DB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_login);

        loginEmail=(EditText)findViewById(R.id.emailField1);
        loginPassword=(EditText)findViewById(R.id.passwordField1);
        loginStudent = (Button)findViewById(R.id.loginStudentButton);

        DB=new DBHelper(this);

        loginStudent.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Log.d("print","print");

                String email=loginEmail.getText().toString();
                String pass=loginPassword.getText().toString();

                if (email.equals("")||(pass.equals(""))){
                    Toast.makeText(StudentLoginActivity.this, "Please enter all the fields", Toast.LENGTH_SHORT).show();
                }
                else {
                    Boolean checkemailpass=DB.checkemailIDpassword(email,pass);
                    if (checkemailpass == true) {

                        SharedPreferences sharedPreferences = getSharedPreferences("MySharedPref", MODE_PRIVATE);
                        SharedPreferences.Editor myEdit = sharedPreferences.edit();

                        // write all the data entered by the user in SharedPreference and apply

                        myEdit.putString("uemail", email);

                      //  myEdit.putString("name", name.getText().toString());
                       // myEdit.putInt("age", Integer.parseInt(age.getText().toString()));
                        myEdit.apply();

                        Toast.makeText(StudentLoginActivity.this, "LogIn Succesfull", Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent(getApplicationContext(),StudentActivity.class);
                        startActivity(intent);
                    }else {
                        Toast.makeText(StudentLoginActivity.this, "Wrong Credentials", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });

       // loginStudent.setOnClickListener(this);

    }

}
