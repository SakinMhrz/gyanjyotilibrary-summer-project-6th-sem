package com.example.gyanjyotilibrary;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import android.content.DialogInterface;


public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText loginEmail, loginPassword;

    private Button btnLogin, btnLoginSignup, studentloginButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        btnLogin = findViewById(R.id.loginButton);
        btnLoginSignup = findViewById(R.id.loginSignupButton);
        studentloginButton = findViewById(R.id.studentloginButton);

        loginEmail=(EditText)findViewById(R.id.emailField);
        loginPassword=(EditText)findViewById(R.id.passwordField);

        btnLogin.setOnClickListener(this);
        btnLoginSignup.setOnClickListener(this);
        studentloginButton.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.loginButton) {

            String inputEmail=loginEmail.getText().toString();
            String inputPassword=loginPassword.getText().toString();
            
            Log.d("inputEmail", inputEmail);
            Log.d("inputPassword", inputPassword);
            if(inputEmail.equals("admin123@gmail.com") && inputPassword.equals("admin")){
                Log.d("Yes","Yes");

                Intent i=new Intent(LoginActivity.this, MainActivity.class);

                startActivity(i);
            }

        }

        if(v.getId() == R.id.loginSignupButton){
            Log.d("Signup", "Clicked Signup");

            startActivity(new Intent(LoginActivity.this, SignUpActivity.class));

        }

        if(v.getId() == R.id.studentloginButton){
            Log.d("Student LOGIN","Clicked Student LOGIN");

            startActivity(new Intent(LoginActivity.this,StudentLoginActivity.class));
        }
    }

}
