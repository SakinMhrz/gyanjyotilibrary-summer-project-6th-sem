package com.example.gyanjyotilibrary;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class ViewBookActivity extends AppCompatActivity {

    TextView book_title_txt, book_author_txt, book_pages_txt;
    Button request_book;

    MyApprovalHelper myDB;

    //EditText title_input,author_input,pages_input;
    //Button update_button, delete_button;

    String id,title,author,pages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_book);

        book_title_txt=(TextView)findViewById(R.id.book_title_txt);
        book_author_txt=(TextView) findViewById(R.id.book_author_txt);
        book_pages_txt=(TextView) findViewById(R.id.book_pages_txt);
        request_book=findViewById(R.id.request_book);
        //delete_button=findViewById(R.id.delete_button);

        //First call this
        getAndSetIntentData();

        //Set ActionBar title after getAndSetIntentData method
        ActionBar ab=getSupportActionBar();
        if (ab != null){
            ab.setTitle(title);
        }

        //update_button.setOnClickListener(new View.OnClickListener() {
          //  @Override
            //public void onClick(View v) {
              //  MyDatabaseHelper myDB=new MyDatabaseHelper(ViewBookActivity.this);
                //Then only call this
                //myDB.updateData(id,title,author,pages);
            //}
       // });

        request_book.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmDialog();
            }
        });

        myDB=new MyApprovalHelper(this);

    }

    void getAndSetIntentData(){
        if (getIntent().hasExtra("id") && getIntent().hasExtra("title") &&
                getIntent().hasExtra("author") && getIntent().hasExtra("pages")){

            //Getting Data from Intent
            id=getIntent().getStringExtra("id");
            title=getIntent().getStringExtra("title");
            author=getIntent().getStringExtra("author");
            pages=getIntent().getStringExtra("pages");

            //Setting Intent Data
            book_title_txt.setText(title);
            book_author_txt.setText(author);
            book_pages_txt.setText(pages);
        }else{
            Toast.makeText(this,"No Data",Toast.LENGTH_SHORT).show();
        }
    }

    void confirmDialog(){
        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        builder.setTitle("Request"+ title + " ?");
        builder.setMessage("Are you sure you want to request this book " + title + " ?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                MyApprovalHelper myDB=new MyApprovalHelper(ViewBookActivity.this);

                SharedPreferences sh = getSharedPreferences("MySharedPref", MODE_PRIVATE);

                String useremail = sh.getString("uemail", "");

                myDB.requestBook(Integer.parseInt(id),useremail);
                finish();

                Toast.makeText(ViewBookActivity.this,"Book Request done Successfully",Toast.LENGTH_SHORT).show();
                Intent intent=new Intent(getApplicationContext(),StudentActivity.class);
                startActivity(intent);
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {

            }
        });
        builder.create().show();
    }
}
