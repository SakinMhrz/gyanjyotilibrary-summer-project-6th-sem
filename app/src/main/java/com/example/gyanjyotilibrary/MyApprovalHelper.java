package com.example.gyanjyotilibrary;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import androidx.annotation.Nullable;

class MyApprovalHelper extends SQLiteOpenHelper {

    private Context context;

    private static final String DATABASE_NAME="Approval.db";
    private static final int DATABASE_VERSION=1;

    private static final String TABLE_NAME="approval";
    private static final String COLUMN_ID="_id";
    private static final String COLUMN_BOOK_ID="book_id";
    private static final String COLUMN_STUDENT_EMAIL="student_email";
    private static final String COLUMN_STATUS="status";

    MyApprovalHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context=context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String query= " CREATE TABLE " + TABLE_NAME +
                " (" + COLUMN_ID +  " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_BOOK_ID + " INTEGER, " +
                COLUMN_STUDENT_EMAIL + " TEXT, " +
                COLUMN_STATUS + " TEXT);";
        db.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int il) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    void requestBook(int book_id, String student_email){
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues cv=new ContentValues();

        cv.put(COLUMN_BOOK_ID,book_id);
        cv.put(COLUMN_STUDENT_EMAIL,student_email);
        cv.put(COLUMN_STATUS,"pending");
        long result=db.insert(TABLE_NAME,null,cv);
        if (result == -1){
            Toast.makeText(context,"Failed", Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(context,"Added Successfully", Toast.LENGTH_SHORT).show();
        }
    }

    Cursor readAllData(){
        String query="SELECT * FROM " + TABLE_NAME;
        SQLiteDatabase db= this.getReadableDatabase();

        Cursor cursor=null;
        if (db !=null){
            cursor=db.rawQuery(query,null);
        }
        return cursor;
    }

    void updateData(String approval_id, String status){
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues cv=new ContentValues();

        cv.put(COLUMN_STATUS,status);
        //cv.put(COLUMN_AUTHOR,author);
        //cv.put(COLUMN_PAGES,pages);

        long result=db.update(TABLE_NAME,cv,"_id=?", new String[]{approval_id});
        if (result==-1){
            Toast.makeText(context,"Fail to Update!",Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(context,"Sucessfully Updated!",Toast.LENGTH_SHORT).show();
        }
    }

    void deleteOneRow(String row_id){
        SQLiteDatabase db=this.getWritableDatabase();
        long result=db.delete(TABLE_NAME,"_id?", new String[]{row_id});
        if (result==-1){
            Toast.makeText(context,"Fail to Delete !",Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(context,"Sucessfully Deleted !",Toast.LENGTH_SHORT).show();
        }
    }

    void deleteAllData(){
        SQLiteDatabase db=this.getWritableDatabase();
        db.execSQL("DELETE FROM "+ TABLE_NAME);
    }
}
